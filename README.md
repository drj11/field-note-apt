# Field Notes - apt

## What package does this file belong to?

    dpkg -S $(which firefox)

I'm using `which firefox` here to find the pathname to the
firefox executable;
this is one of my commonest uses.

## What version of this package do I have installed?

    dpkg -s firefox

Note that there is lots of other output too.

## Where did I get this package from?
